# Todo list

Stuff that are planned to be added/changed.

## Configuration

- [ ] /etc/security/access.conf
- [ ] Filesystem snapshot:
  - [ ] zrepl (rootfs=zfs)
- [ ] Root on tmpfs
- [ ] Filesystem backup (I don't have spare hard drives -_- so not supported for now):
  - [ ] Local incremental backups (to spare disk)
  - [ ] Remote backups
- [ ] incron
- [ ] bees
- [ ] kea as another option for dhcp client
- [ ] booster and dracut options for initramfs_generator
- [ ] `i915.enable_guc=3` (/etc/modprobe.d/kms.conf)
- [ ] turnstile as an alternative to pam-rundir/elogind

## Cosmetic

- [ ] Packer + Terraform / Pulumi (zfs + btrfs VMs) for testing the playbook

## Just in case I forget

- [ ] nftables with rootful podman (<https://github.com/greenpau/cni-plugins>)
- [ ] Write docs about AlpineLinux installation:
  - [ ] BTRFS on LUKS (no encrypted /boot) / ZFS on root
  - [ ] Bootloader configuration:
    - [ ] limine / grub (BTRFS)
    - [ ] gummyboot (a.k.a systemd-boot) / stubbyboot / direct efistub (ZFS)
    - [ ] [APK post-commit hook](https://ptrcnull.me/posts/alpine-commit-hooks/) in the case of gummyboot, stubbyboot, efistub and limine
    - [ ] EFI secure boot (also sign fwupd efi binary)
    - [ ] Add EFI entries for EFI shell and fwupd
  - [ ] Common kernel parameters: `init_on_free=1 page_alloc.shuffle=1 lockdown=integrity quiet`
    - [ ] ZFS: `root=ZFS=rpool/ROOT/alpine`
    - [ ] BTRFS: `modules=sd-mod,usb-storage,btrfs,nvme rootfstype=btrfs cryptroot=UUID=<...> cryptdm=alpine`
