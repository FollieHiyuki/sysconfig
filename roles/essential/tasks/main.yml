---
- name: essential | Change repository URLs
  template:
    src: repositories.j2
    dest: /etc/apk/repositories
    owner: root
    group: root
    mode: '644'

- name: essential | Update repository cache and the system
  community.general.apk:
    available: true
    upgrade: true
    update_cache: true

- name: essential | Install common dependencies
  community.general.apk:
    name: zstd, dbus, font-terminus, shadow-login
    state: present

- name: essential | Enable logging and unicode support for openrc
  lineinfile:
    path: /etc/rc.conf
    state: present
    search_string: '{{ item }}='
    line: '{{ item }}="YES"'
    owner: root
    group: root
    mode: '644'
  loop:
    - rc_logger
    - unicode

# https://wiki.gentoo.org/wiki/Elogind
# elogind still requires 'cgroup-hybrid' useflag
- name: essential | Explicitly enable only cgroup v2 for OpenRC
  lineinfile:
    path: /etc/rc.conf
    state: present
    search_string: rc_cgroup_mode=
    line: rc_cgroup_mode="unified"
    owner: root
    group: root
    mode: '644'
  when: seat_manager != 'elogind'

- name: essential | Change the default motd
  template:
    src: motd.j2
    dest: /etc/motd
    owner: root
    group: root
    mode: '644'

- name: essential | Use zstd for initramfs
  lineinfile:
    path: /etc/mkinitfs/mkinitfs.conf
    state: present
    search_string: initfscomp=
    line: initfscomp="zstd"
    owner: root
    group: root
    mode: '644'
  notify: Regenerate initramfs

- name: essential | Blacklist bluetooth related kernel modules
  community.general.kernel_blacklist:
    name: '{{ item }}'
    state: present
  loop:
    - vivid
    - bluetooth
    - btusb

- name: essential | Use /var/tmp for coredumps
  ansible.posix.sysctl:
    name: kernel.core_pattern
    value: /var/tmp/core-%e.%p.%h.%t
    state: present

- name: essential | Change the tty font to {{ console_font }}
  lineinfile:
    path: /etc/conf.d/consolefont
    state: present
    regexp: '^consolefont='
    line: 'consolefont="{{ console_font }}"'
    owner: root
    group: root
    mode: '644'

- name: essential | Start services on runlevel 'boot'
  service:
    name: '{{ item }}'
    runlevel: boot
    enabled: true
    state: started
  loop: ['consolefont', 'seedrng', 'syslog']

- name: essential | Start services on runlevel 'default'
  service:
    name: '{{ item }}'
    runlevel: default
    enabled: true
    state: started
  loop: ['dbus', 'cgroups']
