#!/bin/sh

PATH="/usr/share/acpid:$PATH"
alias log='logger -t acpid'

# <dev-class>:<dev-name>:<notif-value>:<sup-value>
case "$1:$2:$3:$4" in

button/power:PWRF:* | button/power:PBTN:*)
	log 'Power button pressed'
	poweroff
;;
button/sleep:SLPB:*)
	log 'Sleep button pressed'
	# Suspend to RAM.
	echo mem > /sys/power/state
;;
button/lid:*:close:*)
	log 'Lid closed'
	# Suspend to RAM if AC adapter is not connected.
	[ "$(power-supply -d adapter -s)" = "offline" ] && echo mem > /sys/power/state
;;
ac_adapter:*:*:*0)
	log 'AC adapter unplugged'
	# Suspend to RAM if notebook's lid is closed.
	lid-state -c && echo mem > /sys/power/state
;;
esac

exit 0
